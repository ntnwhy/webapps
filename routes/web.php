<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@index')->name('home');

Route::prefix('/login')->group(function (){
    Route::get('/','LoginController@showLoginForm')->name('login');
    Route::post('/','LoginController@login');
    Route::post('/logout','LoginController@logout')->name('logout');
});

Route::prefix('/master')->group(function () {

    Route::prefix('/gedung')->name('gedung.')->group(function () {
        Route::get('/','GedungController@index')->name('index');
        Route::get('/show','GedungController@show')->name('show');
        Route::get('/form','GedungController@form')->name('form');
        Route::get('/form/{id}','GedungController@form')->name('edit');
        Route::post('/','GedungController@store')->name('store');
        Route::post('/{id}','GedungController@update')->name('update');
        Route::delete('/delete/{id}','GedungController@destroy')->name('delete');
    });

    Route::prefix('/ruang')->name('ruang.')->group(function () {
        Route::get('/','RuangController@index')->name('index');
        Route::get('/show','RuangController@show')->name('show');
        Route::get('/form','RuangController@form')->name('form');
        Route::get('/form/{id}','RuangController@form')->name('edit');
        Route::post('/','RuangController@store')->name('store');
        Route::post('/{id}','RuangController@update')->name('update');
        Route::delete('/delete/{id}','RuangController@destroy')->name('delete');
    });

    Route::prefix('/unit')->name('unit.')->group(function () {
        Route::get('/','UnitController@index')->name('index');
        Route::get('/show','UnitController@show')->name('show');
        Route::get('/form','UnitController@form')->name('form');
        Route::get('/form/{id}','UnitController@form')->name('edit');
        Route::post('/','UnitController@store')->name('store');
        Route::post('/{id}','UnitController@update')->name('update');
        Route::delete('/delete/{id}','UnitController@destroy')->name('delete');
    });

    Route::prefix('/merk')->name('merk.')->group(function () {
        Route::get('/','MerkController@index')->name('index');
        Route::get('/show','MerkController@show')->name('show');
        Route::get('/form','MerkController@form')->name('form');
        Route::get('/form/{id}','MerkController@form')->name('edit');
        Route::post('/','MerkController@store')->name('store');
        Route::post('/{id}','MerkController@update')->name('update');
        Route::delete('/delete/{id}','MerkController@destroy')->name('delete');
    });

    Route::prefix('/kategori')->name('kategori.')->group(function () {
        Route::get('/','KategoriController@index')->name('index');
        Route::get('/show','kategoriController@show')->name('show');
        Route::get('/form','kategoriController@form')->name('form');
        Route::get('/form/{id}','kategoriController@form')->name('edit');
        Route::post('/','kategoriController@store')->name('store');
        Route::post('/{id}','kategoriController@update')->name('update');
        Route::delete('/delete/{id}','kategoriController@destroy')->name('delete');
    });

    Route::prefix('/item')->name('item.')->group(function () {
        Route::get('/','ItemController@index')->name('index');
        Route::get('/show','ItemController@show')->name('show');
        Route::get('/form','ItemController@form')->name('form');
        Route::get('/form/{id}','ItemController@form')->name('edit');
        Route::post('/','ItemController@store')->name('store');
        Route::post('/{id}','ItemController@update')->name('update');
        Route::delete('/delete/{id}','ItemController@destroy')->name('delete');
    });

    Route::prefix('/opsi')->name('opsi.')->group(function () {
        Route::get('/','OpsiController@index')->name('index');
        Route::get('/show','OpsiController@show')->name('show');
        Route::get('/form','OpsiController@form')->name('form');
        Route::get('/form/{id}','OpsiController@form')->name('edit');
        Route::post('/','OpsiController@store')->name('store');
        Route::post('/{id}','OpsiController@update')->name('update');
        Route::delete('/delete/{id}','OpsiController@destroy')->name('delete');
    });

});

Route::prefix('/userman')->group(function () {

    Route::prefix('/level')->name('level.')->group(function () {
        Route::get('/','LevelController@index')->name('index');
        Route::get('/show','LevelController@show')->name('show');
        Route::get('/form','LevelController@form')->name('form');
        Route::get('/form/{id}','LevelController@form')->name('edit');
        Route::post('/','LevelController@store')->name('store');
        Route::post('/{id}','LevelController@update')->name('update');
        Route::delete('/delete/{id}','LevelController@destroy')->name('delete');
    });

    Route::prefix('/user')->name('user.')->group(function () {
        Route::get('/','UserController@index')->name('index');
        Route::get('/show','UserController@show')->name('show');
        Route::get('/form','UserController@form')->name('form');
        Route::get('/form/{id}','UserController@form')->name('edit');
        Route::post('/','UserController@store')->name('store');
        Route::post('/{id}','UserController@update')->name('update');
        Route::delete('/delete/{id}','UserController@destroy')->name('delete');
    });

});

Route::prefix('/aset')->group(function () {

    Route::prefix('/inventarisasi')->name('inventarisasi.')->group(function () {
        Route::get('/','InventarisasiController@index')->name('index');
        Route::get('/show','InventarisasiController@show')->name('show');
        Route::get('/form','InventarisasiController@form')->name('form');
        Route::get('/form/{id}','InventarisasiController@form')->name('edit');
        Route::post('/','InventarisasiController@store')->name('store');
        Route::post('/{id}','InventarisasiController@update')->name('update');
        Route::delete('/delete/{id}','InventarisasiController@destroy')->name('delete');
    });

});
