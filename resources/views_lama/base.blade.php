<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>{{ $title }}</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{ asset('template/global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('template/assets/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('template/global_assets/css/icons/icomoon/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('template/global_assets/css/icons/fontawesome/styles.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('template/global_assets/css/icons/material/styles.min.css') }}" rel="stylesheet" type="text/css">

    @yield('css')

    <!-- Core JS files -->
    <script src="{{ asset('template/global_assets/js/main/jquery.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/main/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/loaders/blockui.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/ui/moment/moment.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/demo_pages/layout_fixed_sidebar_custom.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/notifications/pnotify.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/ui/prism.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/notifications/sweet_alert.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/media/fancybox.min.js') }}"></script>

    <script src="{{ asset('template/assets/js/app.js') }}"></script>

    <script>
        swal.setDefaults({
            buttonsStyling: false,
            confirmButtonClass: 'btn btn-primary',
            cancelButtonClass: 'btn btn-light'
        });

        function rupiah(number) {
            return numeral(number).format('0,0')
        }

        function tanggal(date){
            var timestamp = date;
            return moment(timestamp).format('DD-MM-YYYY');
        }

        function notifWarning(msg) {
            new PNotify({
                title: 'Perhatian !',
                text: msg,
                icon: 'icon-warning22',
                addclass: 'bg-warning border-warning',
                delay: 2000
            });
        }

        function notifSuccess(msg) {
            new PNotify({
                title: 'Berhasil !',
                text: msg,
                icon: 'icon-checkmark3',
                addclass: 'bg-primary',
                delay: 2000
            });
        }

        function goBlock(b){
            $.blockUI({
                showOverlay: b,
                css: {
                    border: 'none',
                    padding: '15px',
                    backgroundColor: '#000',
                    '-webkit-border-radius': '10px',
                    '-moz-border-radius': '10px',
                    opacity: .5,
                    color: '#fff'
                }});
        }
    </script>

    @yield('js')

</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-expand-xl navbar-light navbar-static px-0">
    <div class="d-flex flex-1 pl-3">
        <div class="navbar-brand wmin-0 mr-1">
            <a href="index.html" class="d-inline-block">
                <img src="{{ asset('USB.png') }}" class="d-none d-sm-block" alt="">
                <img src="{{ asset('USBLogo.png') }}" class="d-sm-none" alt="">
            </a>
        </div>
    </div>

    <div class="d-flex w-100 w-xl-auto overflow-auto overflow-xl-visible scrollbar-hidden border-top border-top-xl-0 order-1 order-xl-0">
        <ul class="navbar-nav navbar-nav-underline flex-row text-nowrap mx-auto">
            <li class="nav-item">
                <a href="{{ url('/') }}" class="navbar-nav-link @if($fitur == 'home')active @endif">
                    <i class="icon-home4 mr-2"></i>
                </a>
            </li>

            @if(\Illuminate\Support\Facades\Auth::check())

                @if(\Illuminate\Support\Facades\Auth::user()->level_id == \App\Helper\Constant::LEVEL_SUPERADMIN)

                    <li class="nav-item dropdown nav-item-dropdown-xl">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-grid6 mr-2"></i>
                            Master Data
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-scrollable-xl">
                            <div class="dropdown-header">Lokasi & Unit</div>
                            <div class="dropdown-submenu dropdown-submenu-left">
                                <a href="#" class="dropdown-item dropdown-toggle"><i class="icon-city"></i> Lokasi</a>
                                <div class="dropdown-menu">
                                    <a href="{{ url('/master/gedung') }}" class="dropdown-item @if($fitur == 'gedung')active @endif">Gedung</a>
                                    <a href="{{ url('/master/ruang') }}" class="dropdown-item @if($fitur == 'ruang')active @endif">Ruang</a>
                                </div>
                            </div>
                            <a href="{{ url('/master/unit') }}" class="dropdown-item @if($fitur == 'unit')active @endif">
                                <i class="icon-office"></i>
                                Unit
                            </a>
                            <div class="dropdown-header">Aset</div>
                            <div class="dropdown-submenu dropdown-submenu-left">
                                <a href="#" class="dropdown-item dropdown-toggle"><i class="mi-devices-other"></i> Aset</a>
                                <div class="dropdown-menu">
                                    <a href="{{ url('/master/merk') }}" class="dropdown-item @if($fitur == 'merk')active @endif">Merk</a>
                                    <a href="{{ url('/master/kategori') }}" class="dropdown-item @if($fitur == 'kategori')active @endif">Kategori</a>
                                    <a href="{{ url('/master/item') }}" class="dropdown-item @if($fitur == 'item')active @endif">Item</a>
                                </div>
                            </div>
                            <a href="{{ url('/master/opsi') }}" class="dropdown-item @if($fitur == 'opsi')active @endif">
                                <i class="icon-width"></i>
                                Opsi
                            </a>
                        </div>
                    </li>

                    <li class="nav-item">
                        <a href="{{ url('/workorder') }}" class="navbar-nav-link @if($fitur == 'workorder')active @endif">
                            <i class="mi-work mr-1"></i>
                            Work Orders
                        </a>
                    </li>

                    <li class="nav-item dropdown nav-item-dropdown-xl">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-users mr-1"></i>
                            User
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-scrollable-xl">

                            <a href="{{ url('/userman/level') }}" class="dropdown-item @if($fitur == 'level')active @endif">
                                <i class="icon-width"></i>
                                Level
                            </a>

                            <a href="{{ url('/userman/user') }}" class="dropdown-item @if($fitur == 'user')active @endif">
                                <i class="icon-width"></i>
                                User
                            </a>
                        </div>
                    </li>

                    <li class="nav-item dropdown nav-item-dropdown-xl">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-grid6 mr-2"></i>
                            Aset
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-scrollable-xl">

                            <a href="{{ url('/aset/inventarisasi') }}" class="dropdown-item @if($fitur == 'inventarisasi')active @endif">
                                <i class="icon-width"></i>
                                Inventarisasi
                            </a>
                            <a href="{{ url('/aset/rencana') }}" class="dropdown-item @if($fitur == 'rencana')active @endif">
                                <i class="icon-width"></i>
                                Rencana Maintenance
                            </a>
                            <a href="{{ url('/aset/maintenance') }}" class="dropdown-item @if($fitur == 'maintenance')active @endif">
                                <i class="icon-width"></i>
                                Maintenance
                            </a>
                            <a href="{{ url('/aset/riwayat') }}" class="dropdown-item @if($fitur == 'riwayat')active @endif">
                                <i class="icon-width"></i>
                                Riwayat Aset
                            </a>
                        </div>
                    </li>

                    <li class="nav-item dropdown nav-item-dropdown-xl">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-grid6 mr-2"></i>
                            SIM
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-scrollable-xl">

                            <a href="{{ url('/sim/langganan') }}" class="dropdown-item @if($fitur == 'langganan')active @endif">
                                <i class="icon-width"></i>
                                Langganan
                            </a>
                            <a href="{{ url('/sim/software') }}" class="dropdown-item @if($fitur == 'software')active @endif">
                                <i class="icon-width"></i>
                                Software
                            </a>
                            <a href="{{ url('/sim/ip') }}" class="dropdown-item @if($fitur == 'ip')active @endif">
                                <i class="fas fa-network-wired"></i>
                                IP Address
                            </a>
                            <a href="{{ url('/sim/backup') }}" class="dropdown-item @if($fitur == 'backup')active @endif">
                                <i class="icon-database2"></i>
                                Backup Data
                            </a>
                            <a href="{{ url('/sim/monitoringinternet') }}" class="dropdown-item @if($fitur == 'monitoringinternet')active @endif">
                                <i class="icon-connection"></i>
                                Monitoring Internet
                            </a>

                        </div>
                    </li>

                    <li class="nav-item dropdown nav-item-dropdown-xl">
                        <a href="#" class="navbar-nav-link dropdown-toggle" data-toggle="dropdown">
                            <i class="icon-grid6 mr-2"></i>
                            ISO
                        </a>

                        <div class="dropdown-menu dropdown-menu-right dropdown-scrollable-xl">

                            <div class="dropdown-header">PM & IK</div>
                            <div class="dropdown-submenu dropdown-submenu-left">
                                <a href="#" class="dropdown-item dropdown-toggle"><i class="icon-grid6"></i> Prosedur Mutu</a>
                                <div class="dropdown-menu">
                                    <a href="{{ url('/sim/hardware') }}" class="dropdown-item @if($fitur == 'pmperawatan')active @endif">Perawatan dan Perbaikan</a>
                                    <a href="{{ url('/sim/si') }}" class="dropdown-item @if($fitur == 'pmpengembangan')active @endif">Pengembangan & Layanan SI</a>
                                </div>
                            </div>
                            <div class="dropdown-submenu dropdown-submenu-left">
                                <a href="#" class="dropdown-item dropdown-toggle"><i class="icon-grid6"></i> Instruksi Kerja</a>
                                <div class="dropdown-menu">
                                    <a href="{{ url('/sim/perbaikan') }}" class="dropdown-item @if($fitur == 'ikperbaikan')active @endif">Perbaikan Hardware</a>
                                    <a href="{{ url('/sim/perawatan') }}" class="dropdown-item @if($fitur == 'ikperawatan')active @endif">Perawatan Hardware</a>
                                    <a href="{{ url('/sim/backup') }}" class="dropdown-item @if($fitur == 'ikbackup')active @endif">Backup Database</a>
                                </div>
                            </div>

                        </div>
                    </li>

                @else

                    <li class="nav-item">
                        <a href="{{ url('/workorder') }}" class="navbar-nav-link @if($fitur == 'workorder')active @endif">
                            <i class="mi-work mr-1"></i>
                            Work Orders
                        </a>
                    </li>

                @endif

            @endif

        </ul>
    </div>

    <div class="d-flex flex-xl-1 justify-content-xl-end order-0 order-xl-1 pr-3">
        <ul class="navbar-nav navbar-nav-underline flex-row">

            @if(\Illuminate\Support\Facades\Auth::check())

                @if(\Illuminate\Support\Facades\Auth::user()->level_id == \App\Helper\Constant::LEVEL_SUPERADMIN)


                    <li class="nav-item">
                        <a href="#notifications" class="navbar-nav-link navbar-nav-link-toggler" data-toggle="modal">
                            <i class="icon-bell2"></i>
                            <span class="badge badge-mark border-pink bg-pink"></span>
                        </a>
                    </li>

                 @endif


                    <li class="nav-item nav-item-dropdown-xl dropdown dropdown-user h-100">
                        <a href="#" class="navbar-nav-link navbar-nav-link-toggler d-flex align-items-center h-100 dropdown-toggle" data-toggle="dropdown">
                            <img src="" class="rounded-circle mr-xl-2" height="38" alt="">
                            <span class="d-none d-xl-block">{{ \Illuminate\Support\Facades\Auth::user()->name }}</span>
                        </a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item"><i class="icon-cog5"></i> Ganti Password</a>
                            <a href="#" class="dropdown-item" onclick="event.preventDefault();document.getElementById('logout-form').submit();"><i class="icon-switch2"></i> Logout</a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                        </div>
                    </li>
            @else
                <li class="nav-item">
                    <a href="{{ url('login') }}" class="navbar-nav-link navbar-nav-link-toggler">
                        <button class="btn btn-customizer"> Login </button>
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page content -->
<div class="page-content">
    <!-- Main content -->
    <div class="content-wrapper">

    @yield('content')

    <!-- Footer -->
        <div class="navbar navbar-expand-lg navbar-light border-bottom-0 border-top">
            <div class="text-center d-lg-none w-100">
                <button type="button" class="navbar-toggler dropdown-toggle" data-toggle="collapse" data-target="#navbar-footer">
                    <i class="icon-unfold mr-2"></i>
                    Footer
                </button>
            </div>

            <div class="navbar-collapse collapse" id="navbar-footer">
						<span class="navbar-text">
							&copy; 2021 Universitas Setia Budi Surakarta - Biro Administrasi Akademik dan Sistem Informasi
						</span>

                <ul class="navbar-nav ml-lg-auto">
                    <li class="nav-item"><a href="https://setiabudi.ac.id" class="navbar-nav-link" target="_blank"><span class="text-primary"><i class="icon-globe"></i> Website</span></a></li>
                </ul>
            </div>
        </div>
        <!-- /footer -->
    </div>

</div>
<!-- /page content -->


<!-- Notifications -->
<div id="notifications" class="modal modal-right fade" tabindex="-1" aria-modal="true" role="dialog">
    <div class="modal-dialog modal-dialog-scrollable modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-transparent border-0 align-items-center">
                <h5 class="modal-title font-weight-semibold">Notifications</h5>
                <button type="button" class="btn btn-icon btn-light btn-sm border-0 rounded-pill ml-auto" data-dismiss="modal"><i class="icon-cross2"></i></button>
            </div>

            <div class="modal-body p-0">
                <div class="bg-light text-muted py-2 px-3">New notifications</div>
                <div class="p-3">
                    <div class="d-flex mb-3">
                        <a href="#" class="mr-3">
                            <img src="" width="36" height="36" class="rounded-circle" alt="">
                        </a>
                        <div class="flex-1">
                            <a href="#" class="font-weight-semibold">James</a> has completed the task <a href="#">Submit documents</a> from <a href="#">Onboarding</a> list

                            <div class="bg-light border rounded p-2 mt-2">
                                <label class="custom-control custom-checkbox custom-control-inline mx-1">
                                    <input type="checkbox" class="custom-control-input" checked disabled>
                                    <del class="custom-control-label">Submit personal documents</del>
                                </label>
                            </div>

                            <div class="font-size-sm text-muted mt-1">2 hours ago</div>
                        </div>
                    </div>

                    <div class="d-flex mb-3">
                        <a href="#" class="mr-3">
                            <img src="" width="36" height="36" class="rounded-circle" alt="">
                        </a>
                        <div class="flex-1">
                            <a href="#" class="font-weight-semibold">Margo</a> was added to <span class="font-weight-semibold">Customer enablement</span> channel by <a href="#">William Whitney</a>

                            <div class="font-size-sm text-muted mt-1">3 hours ago</div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mr-3">
                            <div class="bg-danger-100 text-danger rounded-pill">
                                <i class="icon-undo position-static p-2"></i>
                            </div>
                        </div>
                        <div class="flex-1">
                            Subscription <a href="#">#466573</a> from 10.12.2021 has been cancelled. Refund case <a href="#">#4492</a> created

                            <div class="font-size-sm text-muted mt-1">4 hours ago</div>
                        </div>
                    </div>
                </div>

                <div class="bg-light text-muted py-2 px-3">Older notifications</div>
                <div class="p-3">
                    <div class="d-flex mb-3">
                        <a href="#" class="mr-3">
                            <img src="" width="36" height="36" class="rounded-circle" alt="">
                        </a>
                        <div class="flex-1">
                            <a href="#" class="font-weight-semibold">Christine</a> commented on your community <a href="#">post</a> from 10.12.2021

                            <div class="font-size-sm text-muted mt-1">2 days ago</div>
                        </div>
                    </div>

                    <div class="d-flex mb-3">
                        <a href="#" class="mr-3">
                            <img src="" width="36" height="36" class="rounded-circle" alt="">
                        </a>
                        <div class="flex-1">
                            <a href="#" class="font-weight-semibold">Mike</a> added 1 new file(s) to <a href="#">Product management</a> project

                            <div class="bg-light rounded p-2 mt-2">
                                <div class="d-flex align-items-center mx-1">
                                    <div class="mr-2">
                                        <i class="icon-file-pdf text-danger icon-2x position-static"></i>
                                    </div>
                                    <div class="flex-1">
                                        new_contract.pdf
                                        <div class="font-size-sm text-muted">112KB</div>
                                    </div>
                                    <div class="ml-2">
                                        <a href="#" class="btn btn-dark-100 text-body btn-icon btn-sm border-transparent rounded-pill">
                                            <i class="icon-arrow-down8"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div class="font-size-sm text-muted mt-1">1 day ago</div>
                        </div>
                    </div>

                    <div class="d-flex mb-3">
                        <div class="mr-3">
                            <div class="bg-success-100 text-success rounded-pill">
                                <i class="icon-calendar3 position-static p-2"></i>
                            </div>
                        </div>
                        <div class="flex-1">
                            All hands meeting will take place coming Thursday at 13:45. <a href="#">Add to calendar</a>

                            <div class="font-size-sm text-muted mt-1">2 days ago</div>
                        </div>
                    </div>

                    <div class="d-flex mb-3">
                        <a href="#" class="mr-3">
                            <img src="" width="36" height="36" class="rounded-circle" alt="">
                        </a>
                        <div class="flex-1">
                            <a href="#" class="font-weight-semibold">Nick</a> requested your feedback and approval in support request <a href="#">#458</a>

                            <div class="font-size-sm text-muted mt-1">3 days ago</div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mr-3">
                            <div class="bg-primary-100 text-primary rounded-pill">
                                <i class="icon-people position-static p-2"></i>
                            </div>
                        </div>
                        <div class="flex-1">
                            <span class="font-weight-semibold">HR department</span> requested you to complete internal survey by Friday

                            <div class="font-size-sm text-muted mt-1">3 days ago</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- /notifications -->

</body>
</html>
