@extends('base', ["title" => $title , "fitur" => $fitur])

@section('js')
    <script src="{{ asset('template/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/pickers/daterangepicker.js') }}"></script>

    <script>

        $(document).ready(function() {

            $('.select').select2({
                allowClear: true
            });

            $('.tanggal').daterangepicker({
                singleDatePicker: true,
                locale: {
                    format: 'DD-MM-YYYY'
                }
            })

        });

        function save(){

            if ($('#gedung').val() == ''){
                notifWarning('Pilihan Gedung tidak boleh kosong !');
                $('#gedung').focus();

                return;
            }

            if ($('#nama').val() == ''){
                notifWarning('Isian Nama Ruang tidak boleh kosong !');
                $('#nama').focus();

                return;
            }


            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache: false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('aset/inventarisasi') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });

        }

        function batal() {
            window.location.href = '{{ url('aset/inventarisasi') }}';
        }

    </script>

@endsection

@section('content')

    <div class="content-inner">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content container header-elements-md-inline">
                <div class="d-flex">
                    <div class="page-title">
                        <h4 class="font-weight-semibold"><a href="{{ url('/') }}">Home</a><small> {{ $title }} </small> </h4>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content container pt-0">

            <!-- Blocks with chart -->
            <div class="row">
                <div class="card col-lg-12">
                    <div class="card-header header-elements-inline">
                        <h4 class="card-title">Form {{ $title }}</h4>

                    </div>

                    <div class="card-body">

                        <form method="post" id="form" class="form-horizontal">
                            @csrf

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Kode:</label>
                                <div class="col-lg-10">
                                    <input type="text" id="kode" name="kode" class="form-control" placeholder="Kode" value="{{ $form['kode'] }}" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Nama:</label>
                                <div class="col-lg-10">
                                    <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama" value="{{ $form['nama'] }}">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Item:</label>
                                <div class="col-lg-10">
                                    <select name="item" id="item" class="form-control select" data-placeholder="Pilih Item Barang">
                                        <option value=""></option>
                                        @foreach($item as $iitem)

                                            <option value="{{ $iitem->id }}" @if($form['item'] == $iitem->id ) selected @endif> {{ $iitem->kategori->nama }} - {{ $iitem->kode }} {{ $iitem->nama }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Tanggal Beli:</label>
                                <div class="col-lg-4">
                                    <input type="text" id="tgl_beli" name="tgl_beli" class="form-control tanggal" value="{{ $form['tgl_beli'] }}">
                                </div>

                                <label class="col-lg-2 col-form-label">Tanggal Pasang:</label>
                                <div class="col-lg-4">
                                    <input type="text" id="tgl_pasang" name="tgl_pasang" class="form-control tanggal" value="{{ $form['tgl_pasang'] }}" data-date-format="dd-mm-yyyy">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Ruang:</label>
                                <div class="col-lg-10">
                                    <select name="ruang" id="ruang" class="form-control select" data-placeholder="Pilih Lokasi Aset">
                                        <option value=""></option>
                                        @foreach($ruang as $iruang)

                                            <option value="{{ $iruang->id }}" @if($form['ruang'] == $iruang->id ) selected @endif> {{ $iruang->kode }} - {{ $iruang->nama }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Status Aset:</label>
                                <div class="col-lg-10">
                                    <select name="status" id="status" class="form-control select" data-placeholder="Pilih Status Aset">
                                        <option value=""></option>
                                        @foreach($status as $istatus)

                                            <option value="{{ $istatus->id }}" @if($form['status'] == $istatus->id ) selected @endif> {{ $istatus->name }} </option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>


                            <div class="form-group row">
                                <label class="col-lg-2 col-form-label">Keterangan:</label>
                                <div class="col-lg-10">
                                    <input type="text" id="ket" name="ket" class="form-control" placeholder="Keterangan" value="{{ $form['ket'] }}">
                                </div>
                            </div>



                            <div class="row">
                                <div class="col-sm-8 offset-sm-2">
                                    <button type="button" class="btn btn-primary" onclick="save()"> <i class="icon-paperplane"></i> Simpan</button>
                                    <button type="button" class="btn btn-danger" onclick="batal()"> <i class="icon-undo"></i> Batal</button>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

    </div>


@stop
