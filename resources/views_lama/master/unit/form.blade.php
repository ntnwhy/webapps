@extends('base', ["title" => $title , "fitur" => $fitur])

@section('js')
    <script src="{{ asset('template/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>

    <script>

        $(document).ready(function() {

            $('.select').select2({
                allowClear: true
            });

        });

        function save(){

            if ($('#ruang').val() == ''){
                notifWarning('Pilihan Ruang tidak boleh kosong !');
                $('#gedung').focus();

                return;
            }

            if ($('#nama').val() == ''){
                notifWarning('Isian Nama tidak boleh kosong !');
                $('#nama').focus();

                return;
            }

            if ($('#kode').val() == ''){
                notifWarning('Isian Kode tidak boleh kosong !');
                $('#kode').focus();

                return;
            }

            $.ajax({
                url : "{{ $form['url'] }}",
                type: "POST",
                data: $('#form').serialize(),
                cache: false,
                dataType: "json",
                beforeSend:function(request) {
                    goBlock(true);
                },
                success: function(respon){
                    $.unblockUI();

                    if(!respon.status){
                        notifWarning(respon.msg);

                        return;
                    }

                    notifSuccess(respon.msg);

                    window.location.href = '{{ url('master/unit') }}';

                },error: function (jqXHR, textStatus, errorThrown){
                    notifWarning(errorThrown);

                    $.unblockUI();
                }
            });

        }

        function batal() {
            window.location.href = '{{ url('master/unit') }}';
        }

    </script>

@endsection

@section('content')

    <div class="content-inner">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content container header-elements-md-inline">
                <div class="d-flex">
                    <div class="page-title">
                        <h4 class="font-weight-semibold"><a href="{{ url('/') }}">Home</a><small> {{ $title }} </small> </h4>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content container pt-0">

            <!-- Blocks with chart -->
            <div class="row">
                <div class="card col-lg-12">
                    <div class="card-header header-elements-inline">
                        <h4 class="card-title">Form {{ $title }}</h4>

                    </div>

                    <div class="card-body">

                        <form method="post" id="form" class="form-horizontal">
                            @csrf

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="kode">Ruang</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8">
                                    <div class="form-group">
                                        <select name="ruang" id="ruang" class="form-control select">
                                            @foreach($ruang as $iruang)

                                            <option value="{{ $iruang->id }}" @if($form['ruang'] == $iruang->id ) selected @endif> {{ $iruang->kode }} - {{ $iruang->nama }}</option>

                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="kode">Kode</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" id="kode" name="kode" class="form-control" placeholder="Kode Unit Kerja" value="{{ $form['kode'] }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="nama">Nama</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" id="nama" name="nama" class="form-control" placeholder="Nama Unit Kerja" value="{{ $form['nama'] }}">
                                    </div>
                                </div>
                            </div>

                            <div class="row clearfix">
                                <div class="col-lg-2 col-md-2 col-sm-4 form-control-label">
                                    <label for="ket">Keterangan</label>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-8">
                                    <div class="form-group">
                                        <input type="text" id="ket" name="ket" class="form-control" placeholder="Keterangan" value="{{ $form['ket'] }}">
                                    </div>
                                </div>
                            </div>


                            <div class="row clearfix">
                                <div class="col-sm-8 offset-sm-2">
                                    <button type="button" class="btn btn-primary" onclick="save()"> <i class="icon-paperplane"></i> Simpan</button>
                                    <button type="button" class="btn btn-danger" onclick="batal()"> <i class="icon-undo"></i> Batal</button>
                                </div>
                            </div>
                        </form>
                    </div>


                </div>

            </div>

        </div>

    </div>


@stop
