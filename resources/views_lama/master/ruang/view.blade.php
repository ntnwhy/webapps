@extends('base', ["title" => $title, "fitur" => $fitur])

@section("js")

    <script src="{{ asset('template/global_assets/js/plugins/tables/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/plugins/forms/selects/select2.min.js') }}"></script>
    <script src="{{ asset('template/global_assets/js/demo_pages/datatables_advanced.js') }}"></script>

    <script>
        var table;

        $(document).ready(function() {

            initTabel();

            $('.select').select2({
                allowClear: true
            });

        });


        function initTabel(){

            $.extend( $.fn.dataTable.defaults, {
                autoWidth: false,
                ordering: false,
                columnDefs: [
                    {
                        width: 100,
                        targets: [ 0 ]
                    }
                ],
                dom: '<"datatable-header datatable-header-accent"fBl><"datatable-scroll-wrap"t><"datatable-footer"ip>',
                language: {
                    search: '<span>Pencarian:</span> _INPUT_',
                    searchPlaceholder: 'Pencarian',
                    lengthMenu: '<span>Tampilkan :</span> _MENU_',
                    paginate: { 'first': 'Awal', 'last': 'Akhir', 'next': $('html').attr('dir') == 'rtl' ? '&larr;' : '&rarr;', 'previous': $('html').attr('dir') == 'rtl' ? '&rarr;' : '&larr;' }
                },
                lengthMenu: [ 10, 25, 50, 75, 100, 500, 1000 ],
                displayLength: 25,
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Excel <i class="icon-file-excel ml-2"></i>',
                        className: 'btn bg-success',
                        orientation: 'landscape',
                        exportOptions: {
                            stripHtml: true
                        }
                    }
                ]
            });

            table = $('#tabel').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": {
                    "url": "{{ url('master/ruang/show') }}",
                    "type": "GET",
                    beforeSend: function(){
                        goBlock(false);
                    },
                    complete: function () {
                        $.unblockUI();
                    }
                },
                "columns": [
                    {
                        "data": "id",
                        render: function ( data, type, full, meta ) {
                            return '<div class="list-icons">\n' +
                                '                            <div class="dropdown">\n' +
                                '                                <a href="#" class="list-icons-item" data-toggle="dropdown">\n' +
                                '                                    <i class="icon-menu9"></i>\n' +
                                '                                </a>\n' +
                                '\n' +
                                '                                <div class="dropdown-menu dropdown-menu-right">\n' +
                                '                                    <a href="{{ url('master/ruang/form') }}/'+ data +'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>\n' +
                                '                                    <button onclick="hapus('+meta.row+')" class="dropdown-item"><i class="icon-trash"></i> Hapus</button>\n' +
                                '                                </div>\n' +
                                '                            </div>\n' +
                                '                        </div>';
                        }
                    },
                    { 'data': 'gedung.nama'},
                    { 'data': 'kode'},
                    { 'data': 'nama'},
                    { 'data': 'ket'}
                ]
            });

        }

        function reload_table(){
            table.ajax.reload(null, false);
        }

        function tambah(){
            window.location.href = '{{ url('master/ruang/form') }}';
        }

        function hapus(index){

            var id = table.row(index).data();

            swal({
                title: 'Apakah anda yakin?',
                text: "Untuk menghapus data ini!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Ya, hapus!',
                cancelButtonText: 'Tidak, Batalkan!',
                confirmButtonClass: 'btn btn-success',
                cancelButtonClass: 'btn btn-danger',
                buttonsStyling: false,
                allowOutsideClick: false,
                showLoaderOnConfirm: true,
            }).then(function (value) {

                if(value.value){
                    $.ajax({
                        url : "{{ url('master/ruang/delete') }}"+"/"+id.id,
                        type: "DELETE",
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        cache:false,
                        beforeSend:function(request) {
                            goBlock(true);
                        },
                        dataType: "json",
                        success: function(respon){
                            $.unblockUI();

                            reload_table();

                            swal(
                                'Berhasil !',
                                respon.msg,
                                'success'
                            );

                        },error: function (jqXHR, textStatus, errorThrown){
                            $.unblockUI();
                            swal(
                                'Perhatian !!',
                                errorThrown,
                                'danger'
                            );
                        }
                    });
                }
            });
        }
    </script>
@stop

@section("content")

    <div class="content-inner">

        <!-- Page header -->
        <div class="page-header">
            <div class="page-header-content container header-elements-md-inline">
                <div class="d-flex">
                    <div class="page-title">
                        <h4 class="font-weight-semibold"><a href="{{ url('/') }}">Home</a><small> {{ $title }} </small> </h4>
                    </div>
                </div>

            </div>
        </div>
        <!-- /page header -->


        <!-- Content area -->
        <div class="content container pt-0">

            <!-- Blocks with chart -->
            <div class="row">
                <div class="card col-lg-12">
                    <div class="card-header header-elements-inline">
                        <h5 class="card-title">{{ $title }}</h5>

                        <div class="header-elements d-none py-0 mb-3 mb-md-0">
                            <button onclick="reload_table()" type="button" class="btn btn-light"><i class="icon-reload-alt"></i> Refresh </button>
                            <button onclick="tambah()" type="button" class="btn btn-pink ml-3"> <i class="icon-add"></i> Tambah </button>
                        </div>

                    </div>


                    <table id="tabel" class="table table-striped text-nowrap datatable-basic">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>Gedung</th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>Keterangan</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>

                </div>

            </div>

        </div>

    </div>

@stop
