<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public function kategori(){
        return $this->hasOne(KategoriAset::class,'id','kategori_aset_id');
    }

    public function merk(){
        return $this->hasOne(Merk::class,'id','merk_id');
    }
}
