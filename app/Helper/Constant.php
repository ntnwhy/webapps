<?php


namespace App\Helper;


class Constant{
    public const APP_NAME                   = 'SIPIT - Sistem Informasi Pengelolaan IT';

    public const LEVEL_SUPERADMIN           = 1;
    public const LEVEL_USER                 = 2;

    public const LOOK_STATUS_ORDER          = 'Status Order';
    public const LOOK_STATUS_ASET           = 'Status Aset';

}
