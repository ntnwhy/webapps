<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Aset extends Model
{
    public function item(){
        return $this->hasOne(Item::class,'id','item_id');
    }

    public function ruang(){
        return $this->hasOne(Ruang::class,'id','ruang_id');
    }

    public function status(){
        return $this->hasOne(Lookup::class,'id','look_status_aset_id');
    }
}
