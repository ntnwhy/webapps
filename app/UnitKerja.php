<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UnitKerja extends Model
{
    public function ruang(){
        return $this->hasOne(Ruang::class,'id','ruang_id');
    }
}
