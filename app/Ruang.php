<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruang extends Model
{
    public function gedung(){
        return $this->hasOne(Gedung::class,'id','gedung_id');
    }
}
