<?php


namespace App\Http\Controllers;


use App\Level;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "User";
        $data['fitur'] = "user";

        return $this->layout('userman.user.view', $data);
    }

    public function show(Request $request){
        $data = User::query();
        $data->where('isdeleted',false);

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('user.store');
        $form['nama']                = '';
        $form['email']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = User::findOrFail($id);

            $form['url']                 = route('user.update', $id);
            $form['nama']                = $res->nama;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Level";
        $data['fitur'] = "user";
        $data['form']   = $form;

        return $this->layout('userman.user.form', $data);
    }

    public function store(Request $request){
        $data = new User();

        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = User::query()->findOrFail($id);

        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = User::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
