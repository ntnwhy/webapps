<?php


namespace App\Http\Controllers;


use App\Item;
use App\KategoriAset;
use App\Merk;
use Illuminate\Http\Request;

class ItemController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Master Item";
        $data['fitur'] = "item";

        return $this->layout('master.item.view', $data);
    }

    public function show(Request $request){
        $data = Item::query();
        $data->where('isdeleted',false);
        $data->with('kategori');
        $data->with('merk');

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('item.store');
        $form['kode']                = '';
        $form['nama']                = '';
        $form['kategori']            = '';
        $form['merk']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = Item::findOrFail($id);

            $form['url']                 = route('item.update', $id);
            $form['kode']                = $res->kode;
            $form['nama']                = $res->nama;
            $form['kategori']            = $res->kategori_aset_id;
            $form['merk']                = $res->merk_id;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Master Item";
        $data['fitur'] = "item";

        $data['kategori']   = KategoriAset::query()->where('isdeleted', false)->get();
        $data['merk']       = Merk::query()->where('isdeleted', false)->get();

        $data['form']       = $form;

        return $this->layout('master.item.form', $data);
    }

    public function store(Request $request){
        $data = new Item();

        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->kategori_aset_id     = $request->input('kategori');
        $data->merk_id              = $request->input('merk');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Item::query()->findOrFail($id);

        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->kategori_aset_id     = $request->input('kategori');
        $data->merk_id              = $request->input('merk');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = Item::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
