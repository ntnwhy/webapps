<?php

namespace App\Http\Controllers;

use App\Ruang;
use App\UnitKerja;
use Illuminate\Http\Request;

class UnitController  extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Master Unit Kerja";
        $data['fitur'] = "unit";

        return $this->layout('master.unit.view', $data);
    }

    public function show(Request $request){
        $data = UnitKerja::query();
        $data->with('ruang');
        $data->where('isdeleted',false);

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('unit.store');
        $form['ruang']               = '';
        $form['kode']                = '';
        $form['nama']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = UnitKerja::findOrFail($id);

            $form['url']                 = route('unit.update', $id);
            $form['ruang']               = $res->ruang_id;
            $form['kode']                = $res->kode;
            $form['nama']                = $res->nama;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Master Unit Kerja";
        $data['fitur'] = "unit";

        $data['ruang'] = Ruang::query()->where('isdeleted', false)->get();
        $data['form']   = $form;

        return $this->layout('master.unit.form', $data);
    }

    public function store(Request $request){
        $data = new UnitKerja();

        $data->ruang_id            = $request->input('ruang');
        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = UnitKerja::query()->findOrFail($id);

        $data->ruang_id            = $request->input('ruang');
        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = UnitKerja::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
