<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{

    public function index(){

        $data['title'] = "Home";
        $data['fitur'] = "home";

        return $this->layout('home.view', $data);
    }

}
