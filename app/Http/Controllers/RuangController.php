<?php


namespace App\Http\Controllers;


use App\Gedung;
use App\Ruang;
use Illuminate\Http\Request;

class RuangController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Master Ruang";
        $data['fitur'] = "ruang";

        return $this->layout('master.ruang.view', $data);
    }

    public function show(Request $request){
        $data = Ruang::query();
        $data->with('gedung');
        $data->where('isdeleted',false);

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('ruang.store');
        $form['gedung']              = '';
        $form['kode']                = '';
        $form['nama']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = Ruang::findOrFail($id);

            $form['url']                 = route('ruang.update', $id);
            $form['gedung']              = $res->gedung_id;
            $form['kode']                = $res->kode;
            $form['nama']                = $res->nama;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Master Ruang";
        $data['fitur'] = "ruang";

        $data['gedung'] = Gedung::query()->where('isdeleted', false)->get();
        $data['form']   = $form;

        return $this->layout('master.ruang.form', $data);
    }

    public function store(Request $request){
        $data = new Ruang();

        $data->gedung_id            = $request->input('gedung');
        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Ruang::query()->findOrFail($id);

        $data->gedung_id            = $request->input('gedung');
        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = Ruang::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
