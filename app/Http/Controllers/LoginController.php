<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{

    public function showLoginForm(){

        if (Auth::check()) {

            return redirect()->route('home');
        }

        $data['title'] = "Login SIPIT";

        return $this->layout('auth.login', $data);
    }

    public function login(Request $request){
        $this->validator($request);

        if(Auth::attempt(
            $request->only('email','password'),
            $request->filled('remember'))){

            return redirect()
                ->intended(route('home'))
                ->with('status','You are Logged in as Admin!');
        }

        return $this->loginFailed();
    }

    public function logout(){
        Auth::logout();
        Session::flush();

        return redirect()
            ->route('home')
            ->with('status','User has been logged out!');
    }

    private function validator(Request $request){
        $rules = [
            'email'    => 'required|exists:users|min:5|max:191',
            'password' => 'required|string|min:4|max:255',
        ];

        $messages = [
            'email.exists' => 'User tidak ditemukan.',
        ];

        $request->validate($rules,$messages);
    }

    private function loginFailed(){
        return redirect()
            ->back()
            ->withInput()
            ->with('error','Login failed, please try again!');
    }

    public function username(){
        return 'email';
    }

}
