<?php


namespace App\Http\Controllers;


use App\Level;
use Illuminate\Http\Request;

class LevelController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Level";
        $data['fitur'] = "level";

        return $this->layout('userman.level.view', $data);
    }

    public function show(Request $request){
        $data = Level::query();
        $data->where('isdeleted',false);

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('level.store');
        $form['nama']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = Level::findOrFail($id);

            $form['url']                 = route('level.update', $id);
            $form['nama']                = $res->nama;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Level";
        $data['fitur'] = "level";
        $data['form']   = $form;

        return $this->layout('userman.level.form', $data);
    }

    public function store(Request $request){
        $data = new Level();

        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Level::query()->findOrFail($id);

        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = Level::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
