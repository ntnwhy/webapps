<?php


namespace App\Http\Controllers;


use App\Aset;
use App\Helper\Constant;
use App\Item;
use App\KategoriAset;
use App\Lookup;
use App\Ruang;
use Illuminate\Http\Request;

class InventarisasiController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Inventarisasi Aset";
        $data['fitur'] = "inventarisasi";

        $data['kategori'] = KategoriAset::query()
            ->where('isdeleted', false)
            ->get();

        $data['ruang'] = Ruang::query()
            ->with('gedung')
            ->where('isdeleted', false)
            ->get();

        return $this->layout('aset.inventarisasi.view', $data);
    }

    public function show(Request $request){
        $data = Aset::query();
        $data->with('item');
        $data->with('ruang');
        $data->with('status');
        $data->where('isdeleted',false);

        if($request->query('nama')){
            $data->where('nama','like', '%'.$request->query('nama').'%');
        }

        if($request->query('ruang')){
            $data->where('ruang_id', $request->query('ruang'));
        }

        if($request->query('kategori')){
            $data->whereHas('item', function ($sql) use($request) {
                $sql->where('kategori_aset_id', $request->query('kategori'));
            });
        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('inventarisasi.store');
        $form['kode']                = '';
        $form['nama']                = '';
        $form['item']                = '';
        $form['tgl_beli']            = '';
        $form['tgl_pasang']          = '';
        $form['ruang']               = '';
        $form['status']              = '';
        $form['ket']                 = '';

        if ($id){
            $res = Aset::findOrFail($id);

            $form['url']                 = route('inventarisasi.update', $id);
            $form['kode']                = $res->kode;
            $form['nama']                = $res->nama;
            $form['item']                = $res->item_id;
            $form['tgl_beli']            = $res->tgl_beli;
            $form['tgl_pasang']          = $res->tgl_pasang;
            $form['ruang']               = $res->ruang_id;
            $form['status']              = $res->look_status_aset_id;
            $form['ket']                 = $res->ket;

        }

        $data['title'] = "Inventarisasi Aset";
        $data['fitur'] = "inventarisasi";

        $data['item'] = Item::query()
            ->with('kategori')
            ->where('isdeleted', false)
            ->get();

        $data['ruang'] = Ruang::query()
            ->with('gedung')
            ->where('isdeleted', false)
            ->get();

        $data['status'] = Lookup::query()
            ->where('group', Constant::LOOK_STATUS_ASET)
            ->where('isdeleted', false)
            ->get();

        $data['form']   = $form;

        return $this->layout('aset.inventarisasi.form', $data);
    }

    public function store(Request $request){
        $data = new Aset();

        $data->kode               	= $this->getKode($request);
        $data->nama               	= $request->input('nama');
        $data->item_id              = $request->input('item');
        $data->tgl_beli             = date('Y-m-d',strtotime($request->input('tgl_beli')));
        $data->tgl_pasang           = date('Y-m-d',strtotime($request->input('tgl_pasang')));
        $data->ruang_id             = $request->input('ruang');
        $data->look_status_aset_id  = $request->input('status');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Aset::query()->findOrFail($id);

        $data->nama               	= $request->input('nama');
        $data->item_id              = $request->input('item');
        $data->tgl_beli             = date('Y-m-d',strtotime($request->input('tgl_beli')));
        $data->tgl_pasang           = date('Y-m-d',strtotime($request->input('tgl_pasang')));
        $data->ruang_id             = $request->input('ruang');
        $data->look_status_aset_id  = $request->input('status');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = Aset::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

    private function getKode($request){

        $kategori = Item::query()->with('kategori')->where('id', $request->input('item'))->first();

        $data   = Aset::query()
            ->where('kode', 'like', '%'.$kategori->kategori->kode.'%')
            ->max('kode');

        if ($data){
            $urut = explode(".", $data);
            $seq  = (int) $urut[3];
            $seq  = $seq +1;

            return 'SIM-A.'.date('Y', strtotime($request->input('tgl_beli'))).'.'.$kategori->kategori->kode.'.'.substr('00000'.$seq, -5);
        }else{
            return 'SIM-A.'.date('Y', strtotime($request->input('tgl_beli'))).'.'.$kategori->kategori->kode.'.00001';
        }
    }

}
