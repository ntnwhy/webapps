<?php


namespace App\Http\Controllers;


use App\Lookup;
use Illuminate\Http\Request;

class OpsiController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Master Lookup";
        $data['fitur'] = "opsi";

        return $this->layout('master.opsi.view', $data);
    }

    public function show(Request $request){
        $data = Lookup::query();
        $data->where('isdeleted',false);


        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }
        $data->orderBy('group','asc');

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('opsi.store');
        $form['group']                = '';
        $form['nama']                = '';
        $form['value']                 = '';

        if ($id){
            $res = Lookup::findOrFail($id);

            $form['url']                 = route('opsi.update', $id);
            $form['group']               = $res->group;
            $form['nama']                = $res->name;
            $form['value']               = $res->value;
        }

        $data['title'] = "Master Lookup";
        $data['fitur'] = "opsi";

        $data['group']  = Lookup::query()->where('isdeleted', false)->groupBy('group')->get('group');
        $data['form']   = $form;

        return $this->layout('master.opsi.form', $data);
    }

    public function store(Request $request){
        $data = new Lookup();

        $data->group               	= $request->input('group');
        $data->name               	= $request->input('nama');
        $data->value                = $request->input('value');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Lookup::query()->findOrFail($id);

        $data->group               	= $request->input('group');
        $data->name               	= $request->input('nama');
        $data->value                = $request->input('value');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = Lookup::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
