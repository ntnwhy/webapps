<?php

namespace App\Http\Controllers;

use App\WorkOrder;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Yajra\DataTables\Facades\DataTables;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function json($status, $msg, $data = []){

        return response()->json([
            'status' => $status,
            'msg' => $msg,
            'data' => $data,
        ]);
    }

    public function layout($html, $data = [], $marge = []){

        return view($html, $data, $marge);
    }

    public function dataTables($data){

        return DataTables::collection($data)->make(true);
    }

}
