<?php


namespace App\Http\Controllers;


use App\KategoriAset;
use Illuminate\Http\Request;

class KategoriController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Master Kategori";
        $data['fitur'] = "kategori";

        return $this->layout('master.kategori.view', $data);
    }

    public function show(Request $request){
        $data = KategoriAset::query();
        $data->where('isdeleted',false);

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('kategori.store');
        $form['kode']                = '';
        $form['nama']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = KategoriAset::findOrFail($id);

            $form['url']                 = route('kategori.update', $id);
            $form['kode']                = $res->kode;
            $form['nama']                = $res->nama;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Master Kategori";
        $data['fitur'] = "kategori";
        $data['form']   = $form;

        return $this->layout('master.kategori.form', $data);
    }

    public function store(Request $request){
        $data = new KategoriAset();

        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = KategoriAset::query()->findOrFail($id);

        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = KategoriAset::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
