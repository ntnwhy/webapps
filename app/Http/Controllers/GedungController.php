<?php


namespace App\Http\Controllers;


use App\Gedung;
use Illuminate\Http\Request;

class GedungController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Master Gedung";
        $data['fitur'] = "gedung";

        return $this->layout('master.gedung.view', $data);
    }

    public function show(Request $request){
        $data = Gedung::query();
        $data->where('isdeleted',false);

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('gedung.store');
        $form['kode']                = '';
        $form['nama']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = Gedung::findOrFail($id);

            $form['url']                 = route('gedung.update', $id);
            $form['kode']                = $res->kode;
            $form['nama']                = $res->nama;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Master Gedung";
        $data['fitur'] = "gedung";
        $data['form']   = $form;

        return $this->layout('master.gedung.form', $data);
    }

    public function store(Request $request){
        $data = new Gedung();

        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Gedung::query()->findOrFail($id);

        $data->kode               	= $request->input('kode');
        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = Gedung::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
