<?php


namespace App\Http\Controllers;


use App\Merk;
use Illuminate\Http\Request;

class MerkController extends Controller
{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){

        $data['title'] = "Master Merk";
        $data['fitur'] = "merk";

        return $this->layout('master.merk.view', $data);
    }

    public function show(Request $request){
        $data = Merk::query();
        $data->where('isdeleted',false);

        if($request->query('nama')){

            $data->where('nama','like', '%'.$request->query('nama').'%');

        }

        return $this->dataTables($data->get());
    }

    public function form($id = null){

        $form['url']                 = route('merk.store');
        $form['nama']                = '';
        $form['ket']                 = '';

        if ($id){
            $res = Merk::findOrFail($id);

            $form['url']                 = route('merk.update', $id);
            $form['nama']                = $res->nama;
            $form['ket']                 = $res->ket;
        }

        $data['title'] = "Master Merk";
        $data['fitur'] = "merk";
        $data['form']   = $form;

        return $this->layout('master.merk.form', $data);
    }

    public function store(Request $request){
        $data = new Merk();

        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Simpan data berhasil !');
    }

    public function update(Request $request, $id){
        $data = Merk::query()->findOrFail($id);

        $data->nama               	= $request->input('nama');
        $data->ket                	= $request->input('ket');

        $data->save();

        return $this->json(true, 'Update data berhasil !');
    }

    public function destroy($id){
        $data = Merk::query()->findOrFail($id);
        $data->isdeleted = true;
        $data->save();

        return $this->json(true, 'Hapus data berhasil ');
    }

}
