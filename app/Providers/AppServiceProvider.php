<?php

namespace App\Providers;

use App\WorkOrder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $notif = WorkOrder::query()
            ->where('is_done', false)
            ->orderBy('tgl_order','desc')
            ->limit(5)
            ->get();

        View::share('notif', $notif);
    }
}
