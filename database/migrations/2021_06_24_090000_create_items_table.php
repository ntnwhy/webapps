<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->string('kode', 100);
            $table->string('nama', 150);
            $table->foreignId('kategori_aset_id')->unsigned()->constrained('kategori_asets')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('merk_id')->unsigned()->constrained('merks')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
