<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryAsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_asets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('kode', 150);
            $table->dateTime('tgl');
            $table->foreignId('aset_baru_id')->unsigned()->constrained('asets')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('aset_lama_id')->unsigned()->constrained('asets')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_asets');
    }
}
