<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asets', function (Blueprint $table) {
            $table->id();
            $table->string('kode', 150);
            $table->string('nama', 150);
            $table->foreignId('item_id')->unsigned()->constrained('items')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->date('tgl_beli')->nullable();
            $table->date('tgl_pasang')->nullable();
            $table->foreignId('ruang_id')->unsigned()->constrained('ruangs')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('look_status_aset_id')->unsigned()->constrained('lookups')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asets');
    }
}
