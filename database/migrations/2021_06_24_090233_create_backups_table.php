<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBackupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backups', function (Blueprint $table) {
            $table->id();
            $table->string('kode', 100);
            $table->dateTime('tgl');
            $table->foreignId('look_lokasi_backup_id')->unsigned()->constrained('lookups')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('look_jenis_backup_id')->unsigned()->constrained('lookups')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backups');
    }
}
