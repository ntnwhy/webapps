<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKategoriAsetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kategori_asets', function (Blueprint $table) {
            $table->id();
            $table->string('kode', 150);
            $table->string('nama', 150);
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kategori_asets');
    }
}
