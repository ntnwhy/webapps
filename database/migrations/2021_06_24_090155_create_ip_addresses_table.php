<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIpAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ip_addresses', function (Blueprint $table) {
            $table->id();
            $table->string('ip', 150);
            $table->foreignId('aset_id')->unsigned()->constrained('asets')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('look_jenis_ip_id')->unsigned()->constrained('lookups')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('pengguna')->nullable();
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ip_addresses');
    }
}
