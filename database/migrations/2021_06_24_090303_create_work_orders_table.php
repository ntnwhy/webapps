<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWorkOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_orders', function (Blueprint $table) {
            $table->id();
            $table->string('kode', 150);
            $table->foreignId('user_id')->unsigned()->constrained('users')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('keluhan');
            $table->foreignId('look_status_order_id')->unsigned()->constrained('lookups')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->dateTime('tgl_order');
            $table->dateTime('tgl_exe');
            $table->dateTime('tgl_selesai');
            $table->boolean('is_done')->default(false);
            $table->foreignId('petugas_id')->unsigned()->constrained('users')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_orders');
    }
}
