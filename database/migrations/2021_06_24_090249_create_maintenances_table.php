<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaintenancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maintenances', function (Blueprint $table) {
            $table->id();
            $table->string('kode', 150);
            $table->dateTime('tgl');
            $table->foreignId('aset_id')->unsigned()->constrained('asets')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('look_status_aset_id')->unsigned()->constrained('lookups')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('petugas_id')->unsigned()->constrained('users')
                ->onDelete('restrict')->onUpdate('cascade');
            $table->text('ket')->nullable();
            $table->timestamps();
            $table->boolean('isdeleted')->default(false);


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenances');
    }
}
